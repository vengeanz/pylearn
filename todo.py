import attr

@attr.s(auto_attribs=True)
class Todo(object):
    user_id: int
    id: int
    title: str
    completed: bool
