import requests
from todo import Todo

response = requests.get('https://jsonplaceholder.typicode.com/todos/1')

my_todo = Todo(*response.json())

print(f"{response.status_code} {response.json()}")

print(my_todo)
